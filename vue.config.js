module.exports = {
    devServer:{
      host:'localhost',
      port:8080,
      proxy:{
        '/api':{
          target:'https://m.rfyiding.com',
          changeOrigin:true,
          pathRewrite:{
            '^/api':'/api'
          }
        }
      }
    },
    // publicPath:'/app',
    // outputDir:'dist',
    // indexPath:'index2.html',
    // lintOnSave:false,
    productionSourceMap:true,
    chainWebpack:(config)=>{
      config.plugins.delete('prefetch');
    }
  }