import {  post } from './axios'
/**
 * 请求轮播图资源
 */
export  function lunboImgsUrl() {
   let params={"groupIds":["homepage-ntopbanner","homepageMainActivity","homepageTopButton","homepageActivityGroup","homepageInexpensive"],"endpoint":"mobile"}
    return post('/api/rfmobile/banner/listBanner',params)
}