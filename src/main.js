import Vue from 'vue'
import App from './App.vue'
import router from './router'
import VueAwesomeSwiper from 'vue-awesome-swiper'
// import style
import 'swiper/css/swiper.css'
Vue.config.productionTip = false
Vue.use(VueAwesomeSwiper,{})
new Vue({
  render: h => h(App),
  router,
}).$mount('#app')
