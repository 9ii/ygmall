import Vuerouter from 'vue-router'

import Vue from 'vue'

Vue.use(Vuerouter)
import home from './page/home'
export default new Vuerouter({
    routes: [
        {
            path: '',
            redirect:'/home'
        },
        {
            path: '/home',
            component:home
        }
    ]
})